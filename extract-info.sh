#!/bin/bash

# This script extract screenshots and other info from map pk3s and outputs csv

EXTRACT_DIR=/tmp/dumpmaps
STORE_DIR=/tmp/mapshots
PLACEHOLDER_IMG=IMG_PATH
PLACEHOLDER_PK3=PK3_PATH

# $1 = mapinfo file
# $2 = line prompt
function mapinfo_line
{
	grep -m 1 -Ei "^$2 .*" $1 | sed -r -e "s~$2 ([^\r]*).*~\1~i" -e "s/\"/'/g" | tr -cd '[:print:]'
}

# $1 = pk3 name
function extract_info
{
	mkdir -p $EXTRACT_DIR
	mkdir -p $STORE_DIR
	
	unzip -u -d $EXTRACT_DIR $1 'Maps/*' 'maps/*' 'levelshots/*' >/dev/null 2>&1
	
	currentdir=`pwd`
	
	cd $EXTRACT_DIR
	
	if [ -d Maps -a ! -d maps ]
	then
		mv -T Maps maps
	fi
	
	maps=`ls maps/*.bsp 2>/dev/null`
	for map in $maps
	do
		map_name=`basename $map .bsp`
		
		screenshot_dir=maps
		screenshot=`ls $screenshot_dir 2>/dev/null | grep -m 1 -E "^$map_name\.(jpg|jpeg|png|tga)"`
		if [ -z "$screenshot" ]
		then
			screenshot_dir=levelshots
			screenshot=`ls $screenshot_dir 2>/dev/null | grep -m 1 -E "^$map_name\.(jpg|jpeg|png|tga)"`
		fi 
		
		if [ -n "$screenshot" ]
		then
			mv -u $EXTRACT_DIR/$screenshot_dir/$screenshot $STORE_DIR
			screenshot=$PLACEHOLDER_IMG/$screenshot
		fi
		
		if [ -f maps/$map_name.mapinfo ]
		then
			title=`mapinfo_line maps/$map_name.mapinfo title`
			author=`mapinfo_line maps/$map_name.mapinfo author`
			description=`mapinfo_line maps/$map_name.mapinfo description`
		fi
		
		echo \"$map_name\",\"$map_name\",\"$PLACEHOLDER_PK3/`basename $1`\",\"$screenshot\",\"$author\",\"$title\",\"$description\"
	done
	
	cd $currentdir
	
	rm -rf $EXTRACT_DIR
}


function show_help()
{
	echo -e "\e[1mNAME\e[0m"
	echo -e "\t$0 - Extract info from pk3"
	echo
	echo -e "\e[1mSYNOPSIS\e[0m"
	echo -e "\t\e[1m$0\e[0m [\e[4moption\e[0m...] [\e[4;22mfile.pk3\e[0m...]"
	echo -e "\t\e[1m$0\e[0m \e[1mhelp\e[0m|\e[1m-h\e[0m|\e[1m--help\e[0m"
	echo
	echo -e "\e[1mOPTIONS\e[0m"
	echo -e "\t\e[1m-f\e[0m, \e[1m--fields\e[0m"
	echo -e "\t\tOutput field names"
	echo
	echo -e "\e[1mOUTPUT\e[0m"
	echo -e "\tMAP_NAME,GUID,$PLACEHOLDER_PK3/PACKAGE,$PLACEHOLDER_IMG/SCREENSHOT,AUTHOR,TITLE,DESCRIPTION"
	echo -e "\tIt will save the map screenshots in $STORE_DIR"
	echo
	echo -e "\e[1mEXAMPLE\e[0m"
	echo -e "\t$0 -f *.pk3 >maps.csv"
	echo
}
for arg in $@
do
	case $arg in
		*.pk3)
			extract_info $arg
			;;
		help|--help|-h)
			show_help
			exit 0
			;;
		-f|--fields)
			echo title,guid,map_file,map_image,author,title2,description
			;;
	esac
done