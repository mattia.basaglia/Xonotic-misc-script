#!/bin/bash

# Simple script to copy maps from a file

INPUT_DIR=~/.xonotic/data
OUTPUT_DIR=$(pwd)



# @brief Prints a list of files in a pk3
# @param $1 pk3 name
function pk3_files()
{
	unzip -l $1 | head -n -2 | tail -n +4 | sed -r s/^[^a-z]+//
	# zipinfo -1 $1 2>/dev/null
}

dry=false
verbose=true
files=""

for arg in $@
do
	case $arg in
		-i=*)
			eval INPUT_DIR=${arg#*=}
			;;
		-o=*)
			eval OUTPUT_DIR=${arg#*=}
			;;
		--dry|-d|-dry)
			dry=true
			;;
		-q)
			verbose=false
			;;
		*)
			[ -f $arg ] && files=$(cat $arg)
	esac
done

if ! $dry && [ ! -d $OUTPUT_DIR ]
then
	$verbose && echo Creating $OUTPUT_DIR
	mkdir -p $OUTPUT_DIR
fi

for pk3 in $INPUT_DIR/*.pk3
do
	for file in $files
	do
		if pk3_files $pk3 | grep -iq "maps/$file.bsp"
		then
			if $dry
			then
				echo "$pk3 -> $OUTPUT_DIR/"$(basename $pk3)
			else
				$verbose && echo Copying $(basename $pk3) "($file)"
				cp $pk3 $OUTPUT_DIR
			fi
		fi
	done
done 
